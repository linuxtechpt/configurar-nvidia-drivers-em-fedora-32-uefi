# Configurar NVIDIA Drivers em Fedora 32 - UEFI

Sites de ajuda

*  https://www.if-not-true-then-false.com/2015/fedora-nvidia-guide/
*  https://download.nvidia.com/XFree86/Linux-x86_64/430.40/README/installdriver.html#modulesigning
*  https://download.nvidia.com/XFree86/Linux-x86_64/430.40/README/commonproblems.html#nouveau

**Configurar manualmente para modelos compativeis**


> GeForce 6/7 & GeForce 8/9/200/300 & GeForce 400/500/600/700/800/900/10/20 series cards 


**1 - Identificar o kernel**

`$ uname -a `


**2 - Identificar o modelo da Nvidia**

`$ lspci | grep VGA`


**3 - Verificar se temos os drivers nouveau ativos**

`$ lsmod | grep nouveau`


**4 - Instalar dependencias**

`$ dnf install kernel-devel kernel-headers gcc make dkms acpid libglvnd-glx libglvnd-opengl libglvnd-devel pkgconfig`


**5 - Criar dois novos ficheiros de configuration e desabilitar os drivers nouveau no boot**

Ficheiros de configuração padrão:

	- /etc/modprob.d/
	- /usr/lib/modprob.d/

`[$ vi /etc/modprob.d/nvidia-disable-nouveau.conf`

blacklist nouveau
options nouveau modeset=0

`$ vi /usr/lib/modprobe.d/nvidia-disable-nouveau.conf`

blacklist nouveau
options nouveau modeset=0


**6 - Editar grub para desabilitar no boot os drivers nouveau **

`$ vi /etc/default/grub `

Substituir a linha por a que está aqui 

GRUB_CMDLINE_LINUX="resume=/dev/mapper/fedora_kstudio-swap rd.lvm.lv=fedora_kstudio/root rd.lvm.lv=fedora_kstudio/swap rhgb quiet rd.driver.blacklist=nouveau"

**7 - Update grub2**

```
$ su -
$ grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
```



**8 - Download do driver respetivo da NVIDIA **

Ir ao site nvidia.com


**9 - Remover xorg-x11-drv-nouveau**

`dnf remove xorg-x11-drv-nouveau`


**10 - Backup do iniramfs antigo do nouveau e criar nova imagem**

```
$ mv /boot/initramfs-$(uname -r).img /boot/initramfs-$(uname -r)-nouveau.img

$ dracut /boot/initramfs-$(uname -r).img $(uname -r)
```


**11 - Iniciar o sistema em runlevel 3**

```
$ systemctl set-default multi-user.target
$ reboot
```

**NOTA: ANTES DO REBOOT CRIAR UM FICHEIRO TXT E ADICIONAR TODOS OS PASSOS SEGUINTES PARA USAREM O COMANDO CAT E VEREM O QUE FALTA.***


`$ touch configuration.txt`


**12 - Instalar driver NVIDIA**

```
$ cd Transferencias/
$ export PATH="/home/linuxtech/Transferências/:$PATH"
$ chmod +x NVIDIA-Linux-x86_64-440.82.run
$ sudo ./NVIDIA-Linux-x86_64-440.82.run
```


Seguir todo o processo de instalação 

**13 - Ativar modo gráfico runlevel 5**

```
$ systemctl set-default graphical.target
$ reboot
```


